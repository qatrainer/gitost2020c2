<?php

const InputKey = 'myfile';	//<input> name

const AllowedTypes = ['text/xml'];
//deal with errors
if (empty($_FILES[InputKey])) {	//handle error
	die("File Missing!");
}

//if ($_FILES[InputKey]['error'] = 1) { //handle error
//	die("The uploaded file exceeds the upload_max_filesize directive in php.ini");
//}

if ($_FILES[InputKey]['error'] > 0) { //handle error
	die("There was an error with the upload");
}

if (!in_array($_FILES[InputKey]['type'], AllowedTypes)){
	die("Only XML files are allowed");
}

//upload the files

$tmpFile = $_FILES[InputKey]['tmp_name'];

//machine specific  e.g. move the file to the existing folder
$dstFile = 'uploads/'.$_FILES[InputKey]['name'];
echo "file uploaded";

//handle errors
    
if (!move_uploaded_file($tmpFile, $dstFile)) {
	die("Handle Error"); }

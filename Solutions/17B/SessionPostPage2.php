<?php
// Start the session
session_start();
?>

<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Page2</title>
    </head>
    <body>
 <?php
        
 //test whether the $_SESSION superglobal is not empty.               
if(!empty($_SESSION))    
    {
     
       echo "Hello " . $_SESSION['username'] . '<br>'; 
       echo "You favourite colour is " . $_SESSION['colour'] . '<br>'; 
       echo "You favourite animal is " . $_SESSION['animal'] . '<br>';
       echo "<a href='SessionPostPage3.php'>Logout</a><br>";
    }
        
       
  ?>
    </body>
</html>

<?php
// Start the session
session_start();
?>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Logout</title>
    </head>
    <body>
<?php
 // unset the session variables individually

unset($_SESSION['username']);
unset($_SESSION['colour']);
unset($_SESSION['animal']);

//OR unset ALL  session variables with 
//session_unset();
  

//Destroy the session state

session_destroy();
 

//Destroy the session cookie too
setcookie ("PHPSESSID", "", time() - 3600, '/');
 

//test if the $_SESSION superglobal is empty.
if(empty($_SESSION)) 
    {
       echo "<h1>Welcome Guest</h1> " . '<br>'; 
       echo "<a href='SessionPostPage1.php'>Login</a><br>";
    }
     
                
         
        ?>
    </body>
</html>

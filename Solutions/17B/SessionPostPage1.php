<?php
// Start the session
session_start();
?>


<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Page1</title>
    </head>
    <body>

        
        
        <form action="" method="post" >   
            Username: <input type="text" name="username" />  
            Password: <input type="password" name="password" />   
            Colour: <input type="text" name="colour" />   
            Animal: <input type="text" name="animal" />   
            <input type="submit" value="Login" /> 
        </form>
        
<?php
        
//set the session variables if the $_POST superglobal is not empty
        if(!empty($_POST))
            {
            
            $_SESSION["username"] = $_POST["username"];
            $_SESSION["colour"] = $_POST["colour"];
            $_SESSION["animal"] = $_POST["animal"];
        
        
           }
        
           
 // display the user’s name and favourite 
 // colour and animal if the $_SESSION superglobal contains values
        if(!empty($_SESSION))
          {
            
            echo "Welcome " . $_SESSION['username'] . '<br>';  
            echo "You favourite colour is " . $_SESSION['colour'] .'<br>';
            echo "You favourite animal is " . $_SESSION['animal'] .'<br>';
            echo "<a href='SessionPostPage2.php'>Go to Page 2</a><br>"; 
          }
          
        ?>
        
        
    </body>
</html>
<?php

/* 
Prompt the user for a letter and convert to Rock, Paper or Scissors. 
*/

/* 
 Define 3 functions in order, 
 * 1 get the user choice, 
 * 2 convert the choice,
 * 3 show the results.
 */


function get_user_choice() 
{ 
echo "Choose (R)ock, (P)aper, (S)cissors or (Q)uit: "; 
$input = stream_get_line(STDIN, 100, "\n"); 
//convert all inputs to UPPERCASE
$letter = strtoupper(substr($input, 0, 1)); 
$choice = convert_choice($letter); 
return $choice; 

} 


function get_computer_choice() 
{ 
return convert_choice(rand(0,2)); 
} 

/* 
convert a letter (R, P or S) or digit (0, 1, 2) to 
Rock, Paper or Scissors. 
*/

function convert_choice($choice_char) 
{ 
switch ($choice_char) { 
case 'R': 
case '0': 
$choice = 'Rock'; 

break; 

case 'P': 
case '1': 
$choice = 'Paper'; 

break; 

case 'S': 
case '2': 
$choice = 'Scissors'; 

break;

case 'Q': 
    $choice = 'Quit'; 
    die("Thanks for playing , bye!".PHP_EOL); //not the best solution!
break;

default: 
die("Invalid choice: $choice_char".PHP_EOL); 
} 
return $choice; 
} 

function show_result($user_choice, $computer_choice)
{ 
echo PHP_EOL, 
"You chose $user_choice, ", 
"the computer chose $computer_choice...", PHP_EOL; 

sleep(1); // pause for dramatic effect. 

if ($user_choice == 'Rock' && $computer_choice == 'Paper') { 
echo "Computer wins.", PHP_EOL; 
}
elseif ($user_choice == 'Rock' && $computer_choice == 'Scissor') { 
echo "You win.", PHP_EOL; 
}
elseif ($user_choice == 'Paper' && $computer_choice == 'Rock') { 
echo "You win.", PHP_EOL; 
} 
elseif ($user_choice == 'Paper' && $computer_choice == 'Scissors') { 
echo "Computer wins.", PHP_EOL; 
}
elseif ($user_choice == 'Scissors' && $computer_choice == 'Rock') { 
echo "Computer wins.", PHP_EOL; 
} 
elseif ($user_choice == 'Scissors' && $computer_choice == 'Paper') { 
echo "You win.", PHP_EOL; 
} 
else { 
echo "It’s a tie.", PHP_EOL; 
}
} 



//Call the functions in order 
while(true){ 

$user_choice = get_user_choice(); 
$computer_choice = get_computer_choice(); 
show_result($user_choice, $computer_choice);
        
};   


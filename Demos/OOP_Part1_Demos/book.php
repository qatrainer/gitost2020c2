<?php

// Define the Book class.
class Book
{
    // Declare properties.
   public $title;
   public  $author;
   public  $publisher;
   public  $yearOfPublication;
}

function __contructor($title){
    $book->title  =$title;
    
}


// Create a new book instance.
$book = new Book('Game of Thrones');

// Set properties.
//$book->title                = 'Game of Thrones';
$book->author               = 'George R R Martin';
$book->publisher            = 'Voyager Books';
$book->yearOfPublication    = 1996;

// Echo properties.
echo $book->title               . PHP_EOL;
echo $book->author              . PHP_EOL;
echo $book->publisher           . PHP_EOL;
echo $book->yearOfPublication   . PHP_EOL;

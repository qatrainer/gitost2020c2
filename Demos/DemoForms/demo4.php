<!DOCTYPE html>
<html>
<body>

<h2>The select Element</h2>
<p>The select element defines a drop-down list:</p>

<form action="/action_page.php">
    
    username :<input type="text"     name="username" placeholder="type first name" required="yes" />
    surname :<input type="text"     name="usurname" value="smith" readonly="yes"/>
password :<input type="password" name="password" value="" />
<textarea name="comment" rows="10" cols="40">hello from me </textarea>
<br/>

<input type="checkbox" name="milk" checked="yes"/>Milk?

<input type="radio" name="drink" value="tea" />Tea<br />
<input type="radio" name="drink" value="coffee" />Coffee<br />
<input type="radio" name="drink" value="choc" />Chocolate<br />


    <select name="cars">
    <option value="volvo">Volvo</option>
    <option value="saab">Saab</option>
    <option value="fiat">Fiat</option>
    <option value="audi">Audi</option>
  </select>
  <br><br>
  <input type="submit" value="click me">
</form>

</body>
</html>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
  //array count     
$arr_variable = array (10,20,30,40,50);

for ($i=0; $i<count($arr_variable); $i++){
    echo    "The array item is: $arr_variable[$i] <br />";
}


//array sortable

$arr_course = array ("PHP","Java","Python","CSS","HTML");
echo "Array before sorting! <br /><br />";
for ($i=0; $i<count($arr_course); $i++){
    echo    "The array item is: $arr_course[$i] <br />";
}
echo "<br /><br />Array after sorting! <br /><br />";
sort($arr_course);
for ($i=0; $i<count($arr_course); $i++){
    echo    "The array item is: $arr_course[$i] <br />";
}


//
    </body>
</html>

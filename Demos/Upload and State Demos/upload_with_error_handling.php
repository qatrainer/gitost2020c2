<?php

const InputKey = 'myfile';	//<input> name

const AllowedTypes = ['image/jpeg', 'image/jpg'];
//deal with errors
if (empty($_FILES[InputKey])) {	//handle error
	die("File Missing!");
}

if ($_FILES[InputKey]['error'] > 0) { //handle error
	die("Handle the error!");
}

if (!in_array($_FILES[InputKey]['type'], AllowedTypes)){
	die("Handle File Type Not Allowed");
}

//upload the files

$tmpFile = $_FILES[InputKey]['tmp_name'];

//machine specific  e.g. move the file to the existing folder
$dstFile = 'uploads/'.$_FILES[InputKey]['name'];
echo "file uploaded";

//handle errors
if (!move_uploaded_file($tmpFile, $dstFile)) {
	die("Handle Error"); }

<?php

trait Sharable {

public function share($item)  
{  
return 'share this item';  
}

}  
trait Tagme {

public function tag($item)  
{  
return 'tagged  this item';  
}

}  















class Post {

use Sharable;

}

class Comment {

use Sharable, Tagme;

}  



//create new objects

$post1 = new Post;  
echo $post1->share(''); // ‘share this item’

$comment = new Comment;  
echo $comment->share(''); // ‘share this item’  
echo $comment->tag(''); // ‘share this item’  

// A Trait is basically just a way to “copy and paste” code during run time.
<?php

// Interface  
interface Sociable {

public function like();  
public function share();
public function angry();

}

// Trait  
trait Sharable {

public function share($item)  
{  
// share this item  
}

}

// Class  
class Post implements Sociable {

use Sharable;

public function like()  
{  
//  
}

}  


//in this example we have a Sociable interface that states that the Post object is able to like() and share().
//
//The Sharable Trait implements the share() method and the like() method is implemented in the Post class.
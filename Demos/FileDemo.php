<?php


$myFile = "famousWomen.txt";

//Write Demo
$fh = fopen($myFile, 'w') or die("can't open file");
$stringData = "Grace Hopper\n";
fwrite($fh, $stringData);
$stringData = "Mary Wilkes\n";
fwrite($fh, $stringData);
$stringData = "Katherine Johnson\n";
fwrite($fh, $stringData);
fclose($fh);

//Read Demo

$fh = fopen($myFile, 'r');
$filecontents = fread($fh, 5);
//$filecontents = fread($fh, filesize($myFile));
fclose($fh);
echo $filecontents;

//Delete File Demo

// unlink($myFile);


//Append Demo

$fh = fopen($myFile, 'a') or die("can't open file");

$stringData = "Ada Lovelace\n";
fwrite($fh, $stringData);
$stringData = "Annie Easley\n";
fwrite($fh, $stringData);
$stringData = "Radia Perlman\n";
fwrite($fh, $stringData);
fclose($fh);
